#!/usr/bin/python
#encoding: utf-8
import os
import string
import re
import json
import time
import copy
import shutil
from pymorphy import get_morph


class textAnalyzer :
	
	def __init__(self,categoryCount,directory):
		self.morph = get_morph('ru.sqlite-json', 'sqlite')
		self.files = os.listdir(directory);
		self.categoryCount = categoryCount
		self.directory = directory

		if (len(self.files) < categoryCount) :
			print "Указано количество кластеров: %i , но файлов всего: %i. Поэтому категорий будет: %i" % (categoryCount,len(self.files),len(self.files))
			self.categoryCount = len(self.files)

	def analyze(self,show = False):
		wordsInFiles = []
		wordsAll = {}
		for name in self.files :
			file = open(self.directory+name)
			for line in file :
				line = self.prepearText(line)
				words = self.getWords(line)
				wordsInFiles.append(words)
				wordsAll = self.mergeTops(wordsAll,words)
			file.close()

			if (show) :
				words1 = sorted(words.items(), key=lambda (k, v): v, reverse=True)
				print "File: %s" % (name)
				for word in words1[:1] :	
   					try : 
   						os.makedirs("./Result/"+word[0])
   					except :
   						pass   
				shutil.copy2(self.directory+name, './Result/'+word[0]+"/"+name.decode('utf-8'))
				
				for word in words1[:10] :
					print "Word: %s    count: %s" % (word[0],word[1]) 

		wordsAll1 = sorted(wordsAll.items(), key=lambda (k, v): v, reverse=True)
		print "TOTAL"
		for word in wordsAll1[:10] :
			print "Word: %s    count: %s" % (word[0],word[1]) 

	def prepearText(self,line):
		line = line.decode('utf-8').lower()
		line = re.sub(u"[^а-я-]",u" ", line)
		for _ in range(25):
			line = re.sub(u"  ",u" ", line)
		for _ in range(25):
			line = re.sub(u" .{,3} ",u" ", line)
		
		return line

	def getWords(self,line):
		words = line.split(" ")
		worldCount = {}
		for word in words:
			normalizeWord = self.morph.normalize(word.upper()) 
			try :
				key = normalizeWord.pop()
				if worldCount.has_key(key) :
					worldCount[key] = worldCount[key] + 1
				else :
					worldCount[key] = 1
			except :
				True
		return worldCount

	def mergeTops(self,list1,list2):
		if len(list1) == 0 :
			return list2
		else :
			for key in list2:
				if list1.has_key(key) :
					list1[key] = list1[key] + 1
				else :
					list1[key] = 1
			return list1 

		
directory = './Data/'; 
categoryCount = 10;

text = textAnalyzer(categoryCount,directory)
text.analyze(True)

			
